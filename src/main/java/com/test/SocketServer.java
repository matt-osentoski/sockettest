package com.test;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Hello world!
 *
 */
public class SocketServer
{
    public static void main( String[] args ) {
        new SocketServer().startServer();
    }

    public void startServer() {
        final ExecutorService clientProcessingPool = Executors.newFixedThreadPool(10);

        Runnable serverTask = new Runnable() {
            @Override
            public void run() {
                try {
                    ServerSocket serverSocket = new ServerSocket(8000);
                    System.out.println("Waiting for clients to connect...");
                    while (true) {
                        Socket clientSocket = serverSocket.accept();
                        clientProcessingPool.submit(new ClientTask(clientSocket));
                    }
                } catch (IOException e) {
                    System.err.println("Unable to process client request");
                    e.printStackTrace();
                }
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();
    }

    private class ClientTask implements Runnable {
        private final Socket clientSocket;

        private ClientTask(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            String line;
            DataInputStream is;
            PrintStream os;

            long threadId = Thread.currentThread().getId();
            System.out.println("New client on thread: " + threadId);

            try {
                is = new DataInputStream(clientSocket.getInputStream());
                os = new PrintStream(clientSocket.getOutputStream());
                // As long as we receive data, echo that data back to the client.
                while (true) {
                    line = is.readLine();
                    if ("QUIT".equals(line)) {
                        break;
                    }
                    os.println(line);
                }
            }
            catch (IOException e) {
                System.out.println(e);
            }

            try {
                clientSocket.close();
                System.out.println("Client closing on thread: " + threadId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
