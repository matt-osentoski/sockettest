package com.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.*;

/**
 * Created by Matt on 8/20/2015.
 */
public class ClientExample {
    // call our constructor to start the program
    public static void main(String[] args) {
        new ClientExample().startSocketClient(args[0], args[1], args[2]);
    }

    public void startSocketClient(String host, String portString, String file) {
        final String testServerName = host;
        final int port = Integer.parseInt(portString);

        try
        {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                Socket socket = openSocket(testServerName, port);
                DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
                BufferedReader inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                outToServer.writeBytes(line + '\n');
                System.out.println(inFromServer.readLine());

                socket.close();
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    /**
     * Open a socket connection to the given server on the given port.
     * This method currently sets the socket timeout value to 10 seconds.
     * (A second version of this method could allow the user to specify this timeout.)
     */
    private Socket openSocket(String server, int port) throws Exception {
        Socket socket;

        // create a socket with a timeout
        try
        {
            InetAddress inteAddress = InetAddress.getByName(server);
            SocketAddress socketAddress = new InetSocketAddress(inteAddress, port);

            // create a socket
            socket = new Socket();

            // this method will block no more than timeout ms.
            int timeoutInMs = 10*1000;   // 10 seconds
            socket.connect(socketAddress, timeoutInMs);

            return socket;
        }
        catch (SocketTimeoutException ste)
        {
            System.err.println("Timed out waiting for the socket.");
            ste.printStackTrace();
            throw ste;
        }
    }
}
